const path = require('path');

let config = {
    devtool: 'source-map',
    entry: {
        main: './src/main/webapp/javascript/Main.jsx'
    },
    output: {
        filename: "react-app.js",
        path: path.resolve('./src/main/resources/static/dist')
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: "babel-loader",
            options: {
                presets: ['@babel/preset-env', '@babel/preset-react']
            }
        }, {
            test: /\.css$/,
            exclude: /node_modules/,
            loader: "style-loader!css-loader"
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};
console.log('IN CONFIG...', config);

module.exports = config;